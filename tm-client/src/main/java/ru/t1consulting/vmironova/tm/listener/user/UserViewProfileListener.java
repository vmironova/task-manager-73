package ru.t1consulting.vmironova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.dto.request.UserViewProfileRequest;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final UserDTO user = authEndpoint.viewProfileUser(request).getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
