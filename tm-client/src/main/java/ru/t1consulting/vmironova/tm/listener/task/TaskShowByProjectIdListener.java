package ru.t1consulting.vmironova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1consulting.vmironova.tm.event.ConsoleEvent;
import ru.t1consulting.vmironova.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskShowByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskShowByProjectIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final List<TaskDTO> tasks = taskEndpoint.showByProjectIdTask(request).getTasks();
        renderTasks(tasks);
    }

}
