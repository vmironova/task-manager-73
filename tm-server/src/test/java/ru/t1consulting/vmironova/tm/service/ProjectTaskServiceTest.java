package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.configuration.ServerConfiguration;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.exception.entity.ProjectNotFoundException;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.vmironova.tm.listener.JmsLoggerProducer;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;

import static ru.t1consulting.vmironova.tm.constant.ProjectTestData.*;
import static ru.t1consulting.vmironova.tm.constant.TaskTestData.*;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1consulting.vmironova.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @Nullable
    private static IProjectDTOService PROJECT_SERVICE;

    @Nullable
    private static ITaskDTOService TASK_SERVICE;

    @Nullable
    private static IProjectTaskDTOService SERVICE;

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @Nullable
    private static ConfigurableApplicationContext context;

    @BeforeClass
    public static void setUp() throws Exception {
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);
        TASK_SERVICE = context.getBean(ITaskDTOService.class);
        SERVICE = context.getBean(IProjectTaskDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
        context.getBean(JmsLoggerProducer.class).loggerStop();
        context.close();
    }

    @Before
    public void before() throws Exception {
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT1);
        PROJECT_SERVICE.add(USER_ID, USER_PROJECT2);
        TASK_SERVICE.add(USER_ID, USER_TASK1);
        TASK_SERVICE.add(USER_ID, USER_TASK2);
    }

    @After
    public void after() throws Exception {
        TASK_SERVICE.clear(USER_ID);
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.bindTaskToProject(USER_ID, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.bindTaskToProject(USER_ID, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.bindTaskToProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        SERVICE.bindTaskToProject(USER_ID, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeProjectById(null, USER_PROJECT1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeProjectById("", USER_PROJECT1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.removeProjectById(USER_ID, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.removeProjectById(USER_ID, ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.removeProjectById(USER_ID, NON_EXISTING_PROJECT_ID));
        SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK2.getId());
        SERVICE.removeProjectById(USER_ID, USER_PROJECT1.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID, USER_PROJECT1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(USER_ID, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, null, USER_TASK1.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, "", USER_TASK1.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), null));
        Assert.assertThrows(TaskIdEmptyException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), ""));
        Assert.assertThrows(ProjectNotFoundException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, NON_EXISTING_PROJECT_ID, USER_TASK1.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID));
        SERVICE.unbindTaskFromProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(USER_ID, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
        SERVICE.bindTaskToProject(USER_ID, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}
