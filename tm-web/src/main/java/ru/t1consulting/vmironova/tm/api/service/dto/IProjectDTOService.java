package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;
import ru.t1consulting.vmironova.tm.enumerated.Status;

import java.util.List;

public interface IProjectDTOService {

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws Exception;

    @NotNull
    ProjectDTO addByUserId(@Nullable String userId, @NotNull ProjectDTO model) throws Exception;

    void changeProjectStatusById(
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void changeProjectStatusByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    void clear() throws Exception;

    void clearByUserId(@Nullable String userId) throws Exception;

    int count() throws Exception;

    int countByUserId(@Nullable String userId) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @Nullable
    List<ProjectDTO> findAllByUserId(@Nullable String userId) throws Exception;

    @Nullable
    ProjectDTO findOneById(@Nullable String id) throws Exception;

    @Nullable
    ProjectDTO findOneByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable ProjectDTO model) throws Exception;

    void removeByUserId(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    void removeByUserIdAndId(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    ProjectDTO update(@Nullable ProjectDTO model) throws Exception;

    @Nullable
    ProjectDTO updateByUserId(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

    @Nullable
    ProjectDTO updateById(
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @Nullable
    ProjectDTO updateByUserIdAndId(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

}
